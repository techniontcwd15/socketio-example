import { Injectable } from '@angular/core';
import * as io from 'socket.io-client'

@Injectable()
export class SocketioService {
  private socket;

  constructor() {
    this.socket = io('ws://localhost:8988');
    this.socket.on('connect', this.connect.bind(this));
  }

  sendData() {
    this.socket.emit('message', {ok:true});
  }

  connect() {
    console.log('connected');
  }

}
