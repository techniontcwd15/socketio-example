import { Component } from '@angular/core';
import { Socket } from 'ng2-socket-io';
import { SocketioService } from './socketio.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [SocketioService]
})
export class AppComponent {
  title = 'app';

  constructor(private socketio: SocketioService) {

  }

  onclick() {
    this.socketio.sendData();
  }

}
